export function callGetRecordSaga () {
  return {
    type: '@records/GET_RECORDS'
  }
}

export function callNewRecordSaga (payload) {
  return {
    type: '@records/NEW_RECORDS',
    payload: payload
  }
}
