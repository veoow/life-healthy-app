import { combineReducers } from 'redux'

import UserReducer from './user-reducer'
import RecordsReducer from './records-reducer'
import TipsReducer from './tips-reducer'
import UtilitiesReducer from './utilities-reducer'

export default combineReducers({
  UserReducer,
  RecordsReducer,
  TipsReducer,
  UtilitiesReducer
})
