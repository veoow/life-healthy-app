import moment from 'moment-timezone'

const Types = {
  SET_RECORDS: '@records/SET_RECORDS',
  SET_NEW_RECORDS: '@records/SET_NEW_RECORDS'
}

const initialState = {
  records: [],
  nextRecord: null
}

function RecordsReducer (state = initialState, action) {
  switch (action.type) {
    case Types.SET_RECORDS:
      return {
        records: action.payload.records,
        nextRecord: action.payload.next_record
      }
    case Types.SET_NEW_RECORDS:
      return {
        nextRecord: moment().add(2, 'hours').format(),
        records: [
          action.payload,
          ...state.records
        ]
      }
    default:
      return state
  }
}

export default RecordsReducer
