const Types = {
  SET_TIPS: '@tips/SET_TIPS'
}

const initialState = {
  tips: []
}

function TipsReducer (state = initialState, action) {
  switch (action.type) {
    case Types.SET_TIPS:
      return {
        tips: action.payload
      }
    default:
      return state
  }
}

export default TipsReducer
