const Types = {
  LOGIN: '@auth/LOGIN',
  LOGOUT: '@auth/LOGOUT',
  REGISTER: '@auth/LOGOUT'
}

const initialState = {
  isLogged: false,
  token: null,
  user: {},
  error: null
}

function UserReducer (state = initialState, action) {
  switch (action.type) {
    case Types.LOGIN:
      return {
        isLogged: true,
        user: action.payload.user,
        token: action.payload.token,
        error: null
      }
    case Types.REGISTER:
      return {
        isLogged: true,
        user: action.user,
        token: action.token,
        error: null
      }
    case Types.LOGOUT:
      return {
        isLogged: true,
        user: {},
        token: null,
        error: null
      }
    default:
      return state
  }
}

export default UserReducer
