const Types = {
  SET_LOADING: '@util/LOADING'
}

const initialState = {
  loading: false
}

function UtilitiesReducer (state = initialState, action) {
  switch (action.type) {
    case Types.SET_LOADING:
      return {
        loading: action.payload
      }
    default:
      return state
  }
}

export default UtilitiesReducer
