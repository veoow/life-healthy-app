import data from '../../data.json'

export const getTipsApi = () => {
  return new Promise.resolve(data.slider_tips.tips)
}
