import { call, all, takeLatest, put, delay } from 'redux-saga/effects'
import { loginAuthentication } from '../resources/authentication'

function * loginAuth () {
  const response = yield call(loginAuthentication)

  yield put({
    type: '@util/LOADING',
    payload: true
  })

  try {
    if (response) {
      yield put({
        type: '@auth/LOGIN',
        payload: {
          user: response,
          token: '123456@life-healthy'
        }
      })
    }

    yield put({
      type: '@util/LOADING',
      payload: false
    })
  } catch (error) {
    yield put({
      type: '@util/LOADING',
      payload: false
    })
  }
}

export default all([
  takeLatest('@auth/LOGIN_AUTH', loginAuth)
])
