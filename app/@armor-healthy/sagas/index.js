import { all } from 'redux-saga/effects'

import authentication from './authentication'
import recordsSaga from './records-saga'
import tipsSaga from './tips-saga'

export default function * rootSaga () {
  return yield all([
    authentication,
    recordsSaga,
    tipsSaga
  ])
}
