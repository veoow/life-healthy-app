import { call, all, takeLatest, put } from 'redux-saga/effects'
import { getRecordsApi } from '../resources/record-api'

import moment from 'moment-timezone'

function * getRecords () {
  const response = yield call(getRecordsApi)
  yield put({
    type: '@util/LOADING',
    payload: true
  })

  try {
    if (response) {
      yield put({
        type: '@records/SET_RECORDS',
        payload: response
      })
    }

    yield put({
      type: '@util/LOADING',
      payload: false
    })
  } catch (error) {
    yield put({
      type: '@util/LOADING',
      payload: false
    })
  }
}

function * newRecords ({ payload }) {
  yield put({
    type: '@util/LOADING',
    payload: true
  })

  try {
    yield put({
      type: '@records/SET_NEW_RECORDS',
      payload: {
        sugar_blood: payload.bloodSugar,
        date: moment().format(),
        how_you_feel: payload.felling,
        status: 'green',
        workout: payload.workout,
        body_energy: 35.50,
        food: {
          name: 'apple',
          energy: 0.25
        }
      }
    })

    yield put({
      type: '@util/LOADING',
      payload: false
    })
  } catch (error) {
    yield put({
      type: '@util/LOADING',
      payload: false
    })
  }
}

export default all([
  takeLatest('@records/GET_RECORDS', getRecords),
  takeLatest('@records/NEW_RECORDS', newRecords)
])
