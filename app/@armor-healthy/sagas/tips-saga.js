import { call, all, takeLatest, put, delay } from 'redux-saga/effects'
import { getTipsApi } from '../resources/tips-api'

function * getTips () {
  const response = yield call(getTipsApi)

  yield put({
    type: '@util/LOADING',
    payload: true
  })

  try {
    if (response) {
      yield put({
        type: '@tips/SET_TIPS',
        payload: response
      })
    }

    yield put({
      type: '@util/LOADING',
      payload: false
    })
  } catch (error) {
    console.log(error)
    yield put({
      type: '@util/LOADING',
      payload: false
    })
  }
}

export default all([
  takeLatest('@tips/GET_TIPS', getTips)
])
