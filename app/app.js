import React from 'react'
import Routes from './config/Routes'
// import firebase from 'react-native-firebase'

import { StatusBar } from 'react-native'
import { NavigationContainer } from '@react-navigation/native'

import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { store, persistor } from './config/store/index'

import { setStatusBar } from './config/statusBarConfig'

function App () {
  // GET FCM TOKEN
  // useEffect(() => {
  //   firebase.messaging().getToken().then((fcmToken) => {
  //     if (fcmToken) {
  //         console.log('fcmToken', fcmToken)
  //     }
  //   }).catch((err) => console.log('err', err + ' ops'))
  // }, [])

  setStatusBar('transparent', 'dark-style')
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <StatusBar />
        <NavigationContainer>
          <Routes />
        </NavigationContainer>
      </PersistGate>
    </Provider>
  )
};

export default App
