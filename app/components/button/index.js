import React from 'react'
import styled from 'styled-components/native'

import { Text, TouchableOpacity } from 'react-native'

function GradientButton (props) {
  return (
    <ButtonWrapper
      backgroundColor={props.backgroundColor}
      rounded={props.rounded}
      outlined={props.outlined}
      disabled={props.disabled}
      onPress={props.onPress}
    >
      <ButtonText
        color={props.color}
      >
        {props.text}
      </ButtonText>
    </ButtonWrapper>
  )
}

export default GradientButton

export const ButtonWrapper = styled.TouchableOpacity`
  padding: 20px;
  background-color: ${props => props.backgroundColor ? props.backgroundColor : 'transparent'};
  border-radius: ${props => props.rounded ? props.rounded : '0px'};
  border: ${props => props.outlined ? '1px solid #69e6bf' : 'none'}
`
export const ButtonText = styled.Text`
  text-align: center;
  font-weight bold;
  font-size: 16;
  color: ${props => props.color ? props.color : 'grey'}
`
