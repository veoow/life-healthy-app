import React from 'react'
import styled from 'styled-components/native'

import { Text, View } from 'react-native'
import CheckBox from '@react-native-community/checkbox'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'

function CheckBoxInput (props) {
  return (
    <Wrapper
      rounded={props.rounded}
      outlined={props.outlined}
      backgroundColor={props.backgroundColor}
    >
      <View
        style={{
          flex: 1,
          width: '100%',
          flexDirection: 'row',
          justifyContent: 'center',
          alignItems: 'center'
        }}
      >
        {props.icon ? (
          <FontAwesomeIcon
            icon={props.icon}
            size={34}
            color={props.iconColor}
          />
        ) : null}
        {props.text ? (
          <CheckBoxText>{props.text}</CheckBoxText>
        ) : null}
      </View>
      <View style={{ flex: 0.1, flexDirection: 'row', alignItems: 'center' }}>
        <CheckBox
          disabled={props.disabled}
          value={props.value}
          onValueChange={props.onChange(!props.value)}
        />
      </View>
    </Wrapper>
  )
}

export default CheckBoxInput

const Wrapper = styled.View`
  height: 45;
  width: 100%;
  margin-vertical: 10;
  background-color: ${(props) =>
    props.backgroundColor ? props.backgroundColor : 'transparent'};
  border-radius: ${(props) => (props.rounded ? props.rounded : '0px')};
  border: ${(props) => (props.outlined ? '1px solid #69e6bf' : 'none')}
  flex-direction: row;
  justify-content: space-around;
  align-items: center;
`
const CheckBoxText = styled.Text`
  margin-horizontal: 20;
`
