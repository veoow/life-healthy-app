import React from 'react'
import styled from 'styled-components/native'
import moment from 'moment-timezone'

import StatusProvider from '../../providers/injection-provider'

import { Linking } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faPhone, faMeh, faTired, faSmileBeam } from '@fortawesome/free-solid-svg-icons'

function HeaderConfig (props) {
  return (
    <Wrapper>
      <StatusProvider lastInjection={moment(props.records[0].date).format()}>
        {(statusProvided) => {
          switch (statusProvided) {
            case 'green':
              return (
                <StatusWrapper>
                  <Status statusBg='#41C89F'>
                    <FontAwesomeIcon icon={faSmileBeam} size={24} color='white' />
                  </Status>
                </StatusWrapper>
              )
            case 'yellow':
              return (
                <StatusWrapper>
                  <Status statusBg='#EFE010'>
                    <FontAwesomeIcon icon={faMeh} size={24} color='white' />
                  </Status>
                </StatusWrapper>
              )
            case 'red':
              return (
                <StatusWrapper>
                  <Status statusBg='#E64141'>
                    <FontAwesomeIcon icon={faTired} size={24} color='white' />
                  </Status>
                </StatusWrapper>
              )
            default:
              return null
          }
        }}
      </StatusProvider>
      <Avatar source={
        require('../../@armor-healthy/assets/person.jpg')
      }
      />
      <TextName>Olá {props.userInfo.name}</TextName>
      <PhoneButton onPress={() => Linking.openURL('tel:123')}>
        <FontAwesomeIcon icon={faPhone} size={32} color='white' />
      </PhoneButton>
    </Wrapper>
  )
}

export default HeaderConfig

const Wrapper = styled.View`
  flex: 1;
  justifyContent: center;
  alignItems: center;
  height: 100%;
`
const StatusWrapper = styled.View`
  justifyContent: flex-end;
  alignItems: flex-end;
  width: 50%;
`
const Status = styled.View`
  width: 43;
  height: 43;
  border-radius: 50px;
  background-color: ${props => props.statusBg ? props.statusBg : 'transparent'}
  justifyContent: center;
  alignItems: center;
  elevation: 5;
`
const Avatar = styled.Image`
  width: 106;
  height: 106;
  border-radius: 100px;
  border-color: white;
  border-width: 1;
`
const TextName = styled.Text`
  color: white;
  font-size: 15;
  margin-vertical: 10;
  padding: 5px;
`
const PhoneButton = styled.TouchableOpacity`
  background-color: #F83030;
  align-items: center;
  padding: 10px;
  width: 40%;
  border-radius: 5px;
`
