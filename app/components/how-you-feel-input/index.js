import React from 'react'
import styled from 'styled-components'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSmileBeam, faMeh, faTired } from '@fortawesome/free-solid-svg-icons'

function FeelingsInput (props) {
  return (
    <Wrapper>
      <ButtonWrapper>
        <ButtonFeelings
          active={props.value === 'awesome'}
          onPress={() => props.handleChange('awesome')}
        >
          <FontAwesomeIcon icon={faSmileBeam} size={34} color='white' />
          <ButtonFeelingsText>incrível</ButtonFeelingsText>
        </ButtonFeelings>
      </ButtonWrapper>
      <ButtonWrapper>
        <ButtonFeelings
          active={props.value === 'normal'}
          onPress={() => props.handleChange('normal')}
        >
          <FontAwesomeIcon icon={faMeh} size={34} color='white' />
          <ButtonFeelingsText>normal</ButtonFeelingsText>
        </ButtonFeelings>
      </ButtonWrapper>
      <ButtonWrapper>
        <ButtonFeelings
          active={props.value === 'tired'}
          onPress={() => props.handleChange('tired')}
        >
          <FontAwesomeIcon icon={faTired} size={34} color='white' />
          <ButtonFeelingsText>cansado</ButtonFeelingsText>
        </ButtonFeelings>
      </ButtonWrapper>
    </Wrapper>
  )
}

export default FeelingsInput

const Wrapper = styled.View`
  margin-vertical: 10;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`
const ButtonWrapper = styled.View`
  justify-content: center;
  align-items: center;
`
const ButtonFeelings = styled.TouchableOpacity`
  background-color: ${props => props.active ? '#248c6c' : '#43D9AB'};
  border-radius: 10;
  width: 74px;
  height: 74px;
  justify-content: space-around;
  align-items: center;
  padding-vertical: 7;
`
const ButtonFeelingsText = styled.Text`
  color: white;
  font-size: 14;
`
