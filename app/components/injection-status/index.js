import React, { useState, useEffect } from 'react'
import styled from 'styled-components/native'
import { Text } from 'react-native'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSmileBeam, faMeh, faTired } from '@fortawesome/free-solid-svg-icons'

function InjectionStatus (props) {
  const [status, setStatus] = useState({
    icon: faSmileBeam,
    text: 'Everything its ok with you!',
    color: '#41C89F'
  })

  useEffect(() => {
    if (props.status === 'yellow') {
      setStatus({
        icon: faMeh,
        textColor: 'white',
        text: 'Certifique-se de cuidar de si mesmo!',
        color: '#EFE010'
      })
    } else if (props.status === 'red') {
      setStatus({
        icon: faTired,
        textColor: 'white',
        text: 'Atenção! Não deixe sua saúde em segundo plano!',
        color: '#E64141'
      })
    } else {
      setStatus({
        icon: faSmileBeam,
        text: 'Você está atualizado com o seu tratamento!',
        textColor: 'white',
        color: '#41C89F'
      })
    }
  }, [props.status])

  return (
    <InjectionStatusWrapper bgColor={status.color}>
      <InjectionStatusIconWrapper>
        <FontAwesomeIcon icon={status.icon} size={42} color='white' />
      </InjectionStatusIconWrapper>
      <InjectionStatusTextWrapper>
        <InjectionStatusText>{status.text}</InjectionStatusText>
      </InjectionStatusTextWrapper>
    </InjectionStatusWrapper>
  )
}

export default InjectionStatus

export const InjectionStatusWrapper = styled.View`
  elevation: 10;
  margin-vertical: 15;
  flex-direction: row;
  background-color: ${props => `${props.bgColor}`};
  border-radius: 60px;
  width: 100%;
  height: 10%;
  align-items: center;
  justify-content: center;
`
export const InjectionStatusIconWrapper = styled.View`
  flex: 0.3;
  align-items: center;
  justify-content: center;
`
export const InjectionStatusTextWrapper = styled.Text`
  flex: 1
  text-align: left;
`
export const InjectionStatusText = styled.Text`
  font-size: 15;
  color: white;
`
