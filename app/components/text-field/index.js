import React from 'react'
import styled from 'styled-components/native'

function TextField (props) {
  return (
    <Wrapper>
      <Input
        onChangeText={props.handleChange}
        backgroundColor={props.backgroundColor}
        rounded={props.rounded}
        outlined={props.outlined}
        placeholder={props.placeholder}
        onBlur={props.onBlur}
        secureTextEntry={props.secureTextEntry}
        keyboardType={props.keyboardType ? props.keyboardType : 'default'}
      />
    </Wrapper>
  )
}

export default TextField

export const Wrapper = styled.View`
  padding-vertical: 10px;
`
export const Input = styled.TextInput`
  height: 45;
  width: 100%;
  background-color: ${props => props.backgroundColor ? props.backgroundColor : 'transparent'};
  border-radius: ${props => props.rounded ? props.rounded : '0px'};
  border: ${props => props.outlined ? '1px solid #69e6bf' : 'none'}
  text-align: center;
`
