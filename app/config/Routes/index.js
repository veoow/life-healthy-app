import React from 'react'
import { connect } from 'react-redux'
import { createStackNavigator } from '@react-navigation/stack'

import LoginContainer from '../../containers/login-container'
import HomeContainer from '../../containers/home-container'
import ConfigContainer from '../../containers/config-container'
import HistoryContainer from '../../containers/history-container'
import NewRecordContainer from '../../containers/new-record-container'
const Stack = createStackNavigator()

function Routes (props) {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false
      }}
    >
      {!props.isLogged ? (
        <Stack.Screen
          name='Login'
          options={{
            animationTypeForReplace: 'push'
          }}
        >
          {props => <LoginContainer {...props} />}
        </Stack.Screen>
      ) : (
        <>
          <Stack.Screen
            name='Home'
            options={{
              animationTypeForReplace: 'push'
            }}
          >
            {props => <HomeContainer {...props} />}
          </Stack.Screen>

          <Stack.Screen
            name='NewRecord'
            options={{
              animationTypeForReplace: 'push'
            }}
          >
            {props => <NewRecordContainer {...props} />}
          </Stack.Screen>

          <Stack.Screen
            name='Config'
            options={{
              animationTypeForReplace: 'push'
            }}
          >
            {props => <ConfigContainer {...props} />}
          </Stack.Screen>

          <Stack.Screen
            name='History'
            options={{
              animationTypeForReplace: 'push'
            }}
          >
            {props => <HistoryContainer {...props} />}
          </Stack.Screen>
        </>
      )}
    </Stack.Navigator>
  )
}

const mapStateToProps = ({ UserReducer }) => {
  const { isLogged } = UserReducer

  return {
    isLogged
  }
}

export default connect(mapStateToProps, null)(Routes)
