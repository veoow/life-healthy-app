import { StatusBar } from 'react-native'

export function setStatusBar (color, barStyle) {
  StatusBar.setBackgroundColor(color)
  StatusBar.setBarStyle(barStyle)
}
