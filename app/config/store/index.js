import { createStore, applyMiddleware } from 'redux'
import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage'

import createSagaMiddleware from 'redux-saga'

import rootReducer from '../../@armor-healthy/reducers'
import rootSaga from '../../@armor-healthy/sagas'

const persistConfig = {
  key: 'root',
  storage: AsyncStorage
}

const sagaMiddleware = createSagaMiddleware()

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = createStore(
  persistedReducer,
  applyMiddleware(sagaMiddleware),
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

sagaMiddleware.run(rootSaga)

const persistor = persistStore(store)

export { store, persistor }
