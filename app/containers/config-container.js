import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { setStatusBar } from '../config/statusBarConfig'

import ConfigView from '../views/config-view'

function ConfigContainer (props) {
  useEffect(() => {
    setStatusBar('#41C89F', 'light-style')
  }, [])

  return (
    <ConfigView
      style={{ flex: 1 }}
      user={props.user}
      records={props.records}
    />
  )
}

const mapStateToProps = ({ UserReducer, RecordsReducer }) => {
  const { user } = UserReducer
  const { records, nextRecord } = RecordsReducer

  return {
    user,
    records,
    nextRecord
  }
}

export default connect(mapStateToProps, null)(ConfigContainer)
