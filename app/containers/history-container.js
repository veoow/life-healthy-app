import React, { useEffect } from 'react'
import { connect, useDispatch } from 'react-redux'
import { setStatusBar } from '../config/statusBarConfig'

import { Linking } from 'react-native'
import HistoryView from '../views/history-view'

function HistoryContainer (props) {
  useEffect(() => {
    setStatusBar('#41C89F', 'light-style')
  }, [])

  return (
    <HistoryView
      emergencyCall={() => Linking.openURL(`tel:${props.user.emergencyCall}`)}
      userRegisters={props.records}
      user={props.user}
    />
  )
}

const mapStateToProps = ({ UserReducer, RecordsReducer }) => {
  const { user } = UserReducer
  const { records } = RecordsReducer

  return {
    user,
    records
  }
}

export default connect(mapStateToProps, null)(HistoryContainer)
