import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { setStatusBar } from '../config/statusBarConfig'
import * as recordActions from '../@armor-healthy/actions/record-action'
import * as tipsActions from '../@armor-healthy/actions/tips-actions'

import moment from 'moment-timezone'

import { Linking, SafeAreaView } from 'react-native'
import HomeView from '../views/home-view'
import InjectionProvider from '../providers/injection-provider'

function HomeContainer (props) {
  useEffect(() => {
    setStatusBar('#4DA7C9', 'light-style')
  }, [])

  return (
    <>
      <SafeAreaView>
        <InjectionProvider
          lastInjection={props.records.length ? props.records[0].date : moment()}
        >
          {
            (status) => (
              <HomeView
                user={props.user}
                injectionStatus={status}
                navigation={props.navigation}
                emergencyCall={() => Linking.openURL(`tel:${props.user.emergencyCall}`)}
                records={props.records}
                nextRecord={props.nextRecord}
                tips={props.tips ? props.tips : []}
              />
            )
          }
        </InjectionProvider>
      </SafeAreaView>
    </>
  )
}

const mapStateToProps = ({ UserReducer, RecordsReducer, TipsReducer }) => {
  const { user } = UserReducer
  const { records, nextRecord } = RecordsReducer
  const { tips } = TipsReducer

  return {
    user,
    records,
    nextRecord,
    tips
  }
}

const mapDispatchToProps = dispatch => {
  return {
    callGetRecordSaga: () => dispatch(recordActions.callGetRecordSaga()),
    callGetTipsSaga: () => dispatch(tipsActions.callGetTipsSaga())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeContainer)
