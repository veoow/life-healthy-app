import React, { useState } from 'react'
import { connect } from 'react-redux'
import { compose } from 'redux'
import * as yup from 'yup'
import { withFormik } from 'formik'

import FormContext from '../context/form-context'
import * as authActions from '../@armor-healthy/actions/user-action'

import { SafeAreaView } from 'react-native'
import LoginView from '../views/login-view'

function LoginContainer (props) {
  const [state, setState] = useState({
    username: '',
    password: ''
  })

  const handleChange = name => event => {
    setState({
      ...state,
      [name]: event
    })
  }

  const handleEnter = async () => {
    await props.callGetLoginSaga()
  }

  return (
    <SafeAreaView>
      <FormContext.Provider value={{
        values: props.values,
        errors: props.errors,
        setFieldValue: props.setFieldValue,
        handleSubmit: handleEnter,
        isValid: props.isValid
      }}
      >
        <LoginView
          values={state}
          handleChange={handleChange}
        />
      </FormContext.Provider>
    </SafeAreaView>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    callGetLoginSaga: () => dispatch(authActions.getLoginSaga())
  }
}

export default compose(
  connect(null, mapDispatchToProps),
  withFormik({
    mapPropsToValues: () => ({ username: '', password: '' }),

    validationSchema: yup.object().shape({
      username: yup.string()
        .required('Preencha o campo username'),
      password: yup.string()
        .required('Preencha o campo de senha')
    })
  })
)
(LoginContainer)
