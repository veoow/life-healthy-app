import React, { useState } from 'react'
import { connect } from 'react-redux'

import { SafeAreaView, Linking } from 'react-native'
import NewRecordView from '../views/new-records-view'
import * as recordActions from '../@armor-healthy/actions/record-action'

function NewRecordContainer (props) {
  const [state, setState] = useState({
    bloodSugar: '',
    felling: '',
    workout: false,
    eatSomething: false
  })

  const handleChange = (name) => (event) => {
    setState({
      ...state,
      [name]: event
    })
  }

  const handleSubmit = () => {
    props.newRecordSaga(state)
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <NewRecordView
        user={props.user}
        emergencyCall={() => Linking.openURL(`tel:${props.user.emergencyCall}`)}
        onChangeField={handleChange}
        handleSubmit={handleSubmit}
        state={state}
      />
    </SafeAreaView>
  )
}

const mapStateToProps = ({ UserReducer }) => {
  const { user } = UserReducer

  return {
    user
  }
}

const mapDispatchToProps = dispatch => {
  return {
    newRecordSaga: (state) => dispatch(recordActions.callNewRecordSaga(state))
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewRecordContainer)
