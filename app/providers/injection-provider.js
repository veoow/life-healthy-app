import { useCallback, useState, useEffect } from 'react'
import moment from 'moment-timezone'

function InjectionProvider ({ children, lastInjection }) {
  const [status, setStatus] = useState()
  const attPatientStatus = useCallback(() => {
    const injectionTime = moment(lastInjection)
    const now = moment()
    const diff = now.diff(injectionTime, 'hours')

    if (diff > 3 && diff < 5) {
      setStatus('yellow')
    } else if (diff > 5) {
      setStatus('red')
    } else {
      setStatus('green')
    }
  }, [lastInjection])

  useEffect(() => {
    attPatientStatus()
  }, [attPatientStatus])

  return children(status)
}

export default InjectionProvider
