import { useState } from 'react'

function SessionProvider (props) {
  const [tokenIsValid] = useState(false)

  // useEffect(() => {
  //   if (!props.token) {
  //     setTokenIsValid(false)
  //   } else {
  //     // SET API HEADER WITH API
  //     // api.setHeaders({ Authorization: `Bearer ${props.token}` })
  //     setToken(props.token)
  //     setTokenIsValid(true)
  //   }
  // }, [props.token, token])

  return props.children(
    tokenIsValid
  )
}

export default SessionProvider
