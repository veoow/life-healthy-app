import { useState, useEffect } from 'react'

function StatusColorProvider ({ actualStatus, children }) {
  const [status, setStatus] = useState(actualStatus)

  useEffect(() => {
    if (actualStatus === 'red') {
      setStatus({
        name: 'red',
        color: '#E64141'
      })
    } else if (actualStatus === 'yellow') {
      setStatus({
        name: 'yellow',
        color: '#EFE010'
      })
    } else {
      setStatus({
        name: 'green',
        color: '#35C196'
      })
    }
  }, [actualStatus])

  return children(status)
}

export default StatusColorProvider
