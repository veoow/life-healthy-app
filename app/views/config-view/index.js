import React from 'react'
import styled from 'styled-components/native'

import { View, Text } from 'react-native'
import HeaderConfig from '../../components/header-config'
import Footer from '../../@armor-healthy/assets/footer2'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faAddressCard, faUsers, faDoorOpen, faPhoneSquare } from '@fortawesome/free-solid-svg-icons'

function ConfigView (props) {
  return (
    <Wrapper>
      <HeaderConfig userInfo={props.user} records={props.records} />
      <View style={{ alignItems: 'center', flex: 1, height: '100%' }}>
        <View style={{ height: '100%', flex: 1 }}>
          <OptionButton>
            <FontAwesomeIcon style={{ flex: 1, width: '100%' }} icon={faAddressCard} size={32} color='white' />
            <OptionButtonText style={{ flex: 0.5, width: '100%' }}>sobre</OptionButtonText>
          </OptionButton>
          <OptionButton>
            <FontAwesomeIcon style={{ flex: 1, width: '100%' }} icon={faPhoneSquare} size={32} color='white' />
            <OptionButtonText style={{ flex: 0.5, width: '100%' }}>contatos</OptionButtonText>
          </OptionButton>
          <OptionButton>
            <FontAwesomeIcon style={{ flex: 1, width: '100%' }} icon={faUsers} size={32} color='white' />
            <OptionButtonText style={{ flex: 0.5, width: '100%' }}>responsáveis</OptionButtonText>
          </OptionButton>
          <OptionButton>
            <FontAwesomeIcon style={{ flex: 1, width: '100%' }} icon={faDoorOpen} size={32} color='white' />
            <OptionButtonText style={{ flex: 0.5, width: '100%' }}>sair</OptionButtonText>
          </OptionButton>
        </View>
      </View>
      <View style={{ justifyContent: 'flex-end', alignItems: 'center', flex: 0.3, height: '100%' }}>
        <Footer />
      </View>
    </Wrapper>
  )
}

export default ConfigView

const Wrapper = styled.View`
  background-color: #74E2C0; 
  flex: 1;
  height: 100%;
`
const OptionButton = styled.TouchableOpacity`
  background-color: #80F2CF;
  flex-direction: row;
  color: white;
  width: 244px;
  height: 54px;
  border-radius: 10px;
  margin-vertical: 10;
  align-items: center;
  justify-content: space-around;
`
const OptionButtonText = styled.Text`
  color: white;
`
