import React from 'react'
import styled from 'styled-components'
import moment from 'moment-timezone'

import { Text, View } from 'react-native'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faMeh, faTired, faSmileBeam, faCalendarAlt, faCubes, faTachometerAlt, faDumbbell, faUtensils, faSeedling } from '@fortawesome/free-solid-svg-icons'

function HistoryItem (props) {
  const renderStatus = (status = props.status) => {
    switch (status) {
      case 'green':
        return (
          <StatusWrapper>
            <Status statusBg='#41C89F'>
              <StatusIconWrapper>
                <FontAwesomeIcon icon={faSmileBeam} size={40} color='white' />
              </StatusIconWrapper>
              <StatusTextWrapper>
                <StatusText>Você fez este registro com STATUS VERDE</StatusText>
              </StatusTextWrapper>
            </Status>
          </StatusWrapper>
        )
      case 'yellow':
        return (
          <StatusWrapper>
            <Status statusBg='#EFE010'>
              <StatusIconWrapper>
                <FontAwesomeIcon icon={faMeh} size={40} color='white' />
              </StatusIconWrapper>
              <StatusTextWrapper>
                <StatusText>Você fez este registro com STATUS AMARELO</StatusText>
              </StatusTextWrapper>
            </Status>
          </StatusWrapper>
        )
      case 'red':
        return (
          <StatusWrapper>
            <Status statusBg='#E64141'>
              <StatusIconWrapper>
                <FontAwesomeIcon icon={faTired} size={40} color='white' />
              </StatusIconWrapper>
              <StatusTextWrapper>
                <StatusText>Você fez este registro com STATUS VERMELHO</StatusText>
              </StatusTextWrapper>
            </Status>
          </StatusWrapper>
        )
      default:
        return null
    }
  }

  return (
    <HistoryItemWrapper>
      <RegisterWrapper>
        {renderStatus()}
        <RegisterInfoWrapper>
          <RegisterInfoItem>
            <RegisterInfoIconWrapper>
              <FontAwesomeIcon icon={faCalendarAlt} size={23} color='#919191' />
            </RegisterInfoIconWrapper>
            <Text>{moment(props.date).format('DD-MM-YY [ | ] hh:mm')}</Text>
          </RegisterInfoItem>
          <RegisterInfoItem>
            <RegisterInfoIconWrapper>
              <FontAwesomeIcon icon={faCubes} size={23} color='#919191' />
            </RegisterInfoIconWrapper>
            <Text>{props.sugarInBlood} mg/dL</Text>
          </RegisterInfoItem>
          <RegisterInfoItem>
            <RegisterInfoIconWrapper>
              <FontAwesomeIcon icon={faTachometerAlt} size={23} color='#919191' />
            </RegisterInfoIconWrapper>
            <Text>{props.howYouFeel.toUpperCase()}</Text>
          </RegisterInfoItem>
          <RegisterInfoItem>
            <RegisterInfoIconWrapper>
              <FontAwesomeIcon icon={faDumbbell} size={23} color='#919191' />
            </RegisterInfoIconWrapper>
            <Text>{`${props.workout}`}</Text>
          </RegisterInfoItem>
          <RegisterInfoItem>
            <RegisterInfoIconWrapper>
              <FontAwesomeIcon icon={faSeedling} size={23} color='#919191' />
            </RegisterInfoIconWrapper>
            <Text>{props.bodyEnergy ? props.bodyEnergy : '--'}</Text>
          </RegisterInfoItem>
        </RegisterInfoWrapper>
      </RegisterWrapper>
    </HistoryItemWrapper>
  )
}

export default HistoryItem

const HistoryItemWrapper = styled.View`
  padding-horizontal: 20;
`
const RegisterWrapper = styled.View`
  width: 370px;
  height: 80%;
  padding: 20px;
  background-color: #ffffff;
  elevation: 2;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border-radius: 10px;
`
const StatusWrapper = styled.View`
  flex: 0.2;
  justifyContent: flex-start;
  alignItems: flex-end;
  width: 100%;
`
const Status = styled.View`
  width: 100%;
  height: 62px;
  border-radius: 10px;
  background-color: ${props => props.statusBg ? props.statusBg : 'transparent'}
  justifyContent: center;
  alignItems: center;
  elevation: 5;
  flex-direction: row;
  padding: 20px;
`
const StatusIconWrapper = styled.View`
  flex: 0.3;
`
const StatusTextWrapper = styled.View`
  flex: 1;
`
const StatusText = styled.Text`
  color: white;
  font-size: 15;
  font-weight: bold;
  padding: 20px;
`
const RegisterInfoWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
  height: 100%;
  background-color: #F6F6F6;
  border-radius: 10px;
  padding: 10px;
`
const DeseaseIncrease = styled.View`
  position: relative;
  
`
const RegisterInfoItem = styled.View`
  background-color: #E2E2E2;
  border-radius: 10px;
  padding: 20px;
  margin-vertical: 5;
  flex-direction: row;
  align-items: center;
`
const RegisterInfoIconWrapper = styled.View`
  background-color: #ffffff;
  padding: 10px;
  border-radius: 20px;
  margin-right: 20px;
`
