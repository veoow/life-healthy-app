import React from 'react'
import styled from 'styled-components/native'
import { View } from 'react-native'

import HeaderToolbar from '../home-view/components/header-toolbar'
import HistoryItem from './components/history-item'

function HistoryView (props) {
  return (
    <View style={{ flex: 1, height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center' }}>
      <HeaderToolbar emergencyCall={props.emergencyCall} userInfo={props.user} />
      <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
        <HistoryTitle>Histórico</HistoryTitle>
      </View>
      <HistoryWrapper
        horizontal
        scrollEventThrottle={200}
        decelerationRate='fast'
        pagingEnabled
        showsHorizontalScrollIndicator={false}
      >
        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row' }}>
          {
            props.userRegisters.map(register => (
              <HistoryItem
                key={register.id}
                date={register.date}
                status={register.status}
                sugarInBlood={register.sugar_blood}
                howYouFeel={register.how_you_feel}
                workout={register.workout}
                bodyEnergy={register.body_energy}
                food={register.food}
              />
            ))
          }
        </View>
      </HistoryWrapper>
    </View>
  )
}

export default HistoryView

const HistoryWrapper = styled.ScrollView`
  flex: 1;
  width: 100%;
  height: 100%;
`
const HistoryTitle = styled.Text`
  color: #35C196;
  font-size: 30;
  font-weight: bold;
`
