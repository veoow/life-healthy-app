import React, { useState } from 'react'
import styled from 'styled-components/native'

import moment from 'moment-timezone'

import { Dimensions, Animated } from 'react-native'
import Svg, { Circle, Text } from 'react-native-svg'

// Total circle 283
const AnimatedCircle = Animated.createAnimatedComponent(Circle)
function Alarm (props) {
  const injectionTime = moment().add(2, 'hours')
  const now = moment()

  let diff = injectionTime.diff(now, 'minutes')
  const [progressTimer, setProgressTimer] = useState((280 * diff) / 100)

  setInterval(() => {
    const now = moment()
    diff = injectionTime.diff(now, 'minutes')
    setProgressTimer((280 * diff) / 100)
  }, 30000)

  return (
    <Wrapper>
      <AlarmDescriptionWrapper>
        <AlarmDescription>
          Não se esqueça de tomar sua medicação ás...
        </AlarmDescription>
      </AlarmDescriptionWrapper>
      <AlarmWrapper>
        <Svg height='80%' width='80%' viewBox='0 0 100 100'>
          <Circle
            cx='50'
            cy='50'
            r='45'
            stroke='#4DA7C9'
            strokeWidth='4.5'
            fill='none'
          />
          <AnimatedCircle
            cx='50'
            cy='50'
            r='45'
            stroke='#00587A'
            strokeWidth='7'
            strokeDasharray={`${progressTimer}, 900`}
            strokeLinejoin='round'
            strokeLinecap='round'
            fill='none'
          />
          <Text
            fill='#4DA7C9'
            fontSize='22'
            x='50'
            y='60'
            textAnchor='middle'
          >
            {diff <= 0 ? 'Agora' : moment(injectionTime).format('hh:mm')}
          </Text>
        </Svg>
      </AlarmWrapper>
    </Wrapper>
  )
}

export default Alarm

export const Wrapper = styled.View`
  flex-direction: row;
  background-color: white;
  margin-vertical: 15;
  margin-horizontal: 5;
  padding: 10px;
  height: 149px;
  justify-content: center;
  align-items: center;
  width: ${Dimensions.get('window').width - 40}
  border-radius: 20px;
  flex: 1;
`
export const AlarmDescriptionWrapper = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 100%;
`
export const AlarmDescription = styled.Text`
  font-size: 16;
  color: #4DA7C9;
`
export const AlarmWrapper = styled.View`
  flex-direction: row;
  flex: 1;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
`
