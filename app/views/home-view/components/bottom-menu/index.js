import React from 'react'
import styled from 'styled-components/native'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { menuOptions } from './menu-options'

function BottomMenu (props) {
  return (
    <BottomMenuWrapper>
      {
        menuOptions.map((item) => (
          <MenuOption key={item.value} onPress={() => item.action(props.navigation)}>
            <FontAwesomeIcon icon={item.icon} size={25} color='white' />
            <MenuOptionText>{item.title}</MenuOptionText>
          </MenuOption>
        ))
      }
    </BottomMenuWrapper>
  )
}

export default BottomMenu

export const BottomMenuWrapper = styled.View`
  width: 100%;
  height: 100%;
  flex: 1;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`

export const MenuOption = styled.TouchableOpacity`
  background-color: #4DA7C9;
  padding-vertical: 10;
  width:  75px;
  height: 75px;
  border-radius: 5px;
  elevation: 5;
  align-items: center;
  justify-content: space-around;
`

export const MenuOptionText = styled.Text`
  color: white;
`
