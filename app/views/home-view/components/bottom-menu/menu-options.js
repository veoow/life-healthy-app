import { faNotesMedical, faFileMedicalAlt, faChartLine, faCog } from '@fortawesome/free-solid-svg-icons'

export const menuOptions = [
  {
    title: 'adicionar',
    value: 'add',
    icon: faNotesMedical,
    action: (navigation) => navigation.navigate('NewRecord')
  },
  {
    title: 'config',
    value: 'config',
    icon: faCog,
    action: (navigation) => navigation.navigate('Config')
  },
  {
    title: 'registros',
    value: 'medical_registers',
    icon: faFileMedicalAlt,
    action: (navigation) => navigation.navigate('History')
  },
  {
    title: 'gráficos',
    value: 'performance',
    icon: faChartLine,
    action: () => {}
  }
]
