import React from 'react'
import styled from 'styled-components/native'

import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faSmileBeam } from '@fortawesome/free-solid-svg-icons'

import StatusProvider from '../../../../providers/status-color-provider'

function HeaderToolbar (props) {
  return (
    <Wrapper>
      <StatusProvider actualStatus='green'>
        {(status) => (
          <StatusWrapper>
            <Status statusColor={status.color}>
              <FontAwesomeIcon icon={faSmileBeam} size={22} color='white' />
              <StatusText>Status {status.name}</StatusText>
            </Status>
          </StatusWrapper>
        )}
      </StatusProvider>
      <AvatarWrapper>
        <AvatarSaudation>Olá {props.userInfo.name}</AvatarSaudation>
        <Avatar source={
          require('../../../../@armor-healthy/assets/person.jpg')
        }
        />
      </AvatarWrapper>
    </Wrapper>
  )
}

export default HeaderToolbar

const Wrapper = styled.View`
  height: 75px;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`
const StatusWrapper = styled.View`
  padding: 20px;
  flex: 1;
`
const Status = styled.View`
  background-color: ${props => props.statusColor ? props.statusColor : '#4DA7C9'};
  flex-direction: row;
  align-items: center;
  justify-content: center;
  padding: 10px;
  width: 100%;
  height: 44px;
  border-radius: 5px;
`
const StatusText = styled.Text`
  font-size: 15;
  color: white;
  margin-horizontal: 5;
`
const AvatarWrapper = styled.View`
  width: 100%;
  padding: 10px;
  flex: 1.5;
  flex-direction: row;
  align-items: center;
  justify-content: flex-end;
`
const AvatarSaudation = styled.Text`
  color: #4DA7C9;
  font-size: 12;
`
const Avatar = styled.Image`
  width: 45;
  height: 45;
  border-radius: 50px;
  margin-horizontal: 10;
  border-color: white;
  border-width: 1;
`
