import React from 'react'
import styled from 'styled-components/native'

import { FlatList } from 'react-native'
import SliderItem from './slider-item'

function HomeSlider (props) {
  const sliderData = [
    { type: 'alarm', active: true },
    { type: 'tips', active: false }
  ]
  return (
    <HomeSliderWrapper>
      <FlatList
        keyExtractor={item => item.type}
        horizontal
        pagingEnabled
        data={sliderData}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <SliderItem
            item={item.type}
            status={props.status}
            nextRegister={props.nextRegister}
            registers={props.registers}
            index={index}
            tips={props.tips}
          />
        )}
      />
    </HomeSliderWrapper>
  )
}

export default HomeSlider

export const HomeSliderWrapper = styled.View`
  padding-vertical: 0;
  width: 100%;
  height: 100%;
  flex: 2;
`
export const SliderIndicatorWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
`
export const SliderIndicator = styled.View`
  background-color: transparent;
  width: 12px;
  height: 12px;
  border-width: 1px;
  border-color: #00587A;
  border-radius: 50px;
  margin-horizontal: 5;
`
