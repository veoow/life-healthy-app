import React from 'react'
import styled from 'styled-components/native'

import { Dimensions } from 'react-native'
import StatusColorProvider from '../../../../providers/status-color-provider'
import Tips from './tips'

function SliderItem (props) {
  const RenderItem = () => {
    switch (props.item) {
      case 'tips':
        return (
          <Tips selectedTip={props.tips} />
        )
      default:
        return <></>
    }
  }

  return (
    <StatusColorProvider actualStatus={props.status}>
      {(statusInfo) => (
        <SliderItemWrapper statusColor={statusInfo}>
          {RenderItem()}
        </SliderItemWrapper>
      )}
    </StatusColorProvider>
  )
}

export default SliderItem

export const SliderItemWrapper = styled.View`
  background-color: #4DA7C9;
  margin-vertical: 20;
  margin-horizontal: 5;
  height: 149px;
  justify-content: center;
  align-items: center;
  width: ${Dimensions.get('window').width - 40}
  border-radius: 20px;
  flex: 1;
`
