import React from 'react'
import styled from 'styled-components/native'

import { Linking } from 'react-native'

function Tips (props) {
  return (
    <Wrapper>
      <TipsWrapper>
        <TipsText>
          {props.selectedTip && props.selectedTip.content}
        </TipsText>
        <TipsAuthorCredits>
          - {props.selectedTip && props.selectedTip.author}
        </TipsAuthorCredits>
      </TipsWrapper>
      <SeeMoreWrapper>
        <SeeMoreButton onPress={() => Linking.openURL(props.selectedTip && props.selectedTip.link)}>
          <SeeMoreButtonText> ver mais + </SeeMoreButtonText>
        </SeeMoreButton>
      </SeeMoreWrapper>
    </Wrapper>
  )
}

export default Tips

export const Wrapper = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  justify-content: center;
  align-items: center;
  background-color: #4DA7C9;
  margin-vertical: 15;
  border-radius: 20px;
`
export const TipsWrapper = styled.View`
  flex: 1;
  padding-horizontal: 15;
  justify-content: center;
`
export const TipsText = styled.Text`
  font-size: 15;
  color: white;
  text-align: left;
`
export const TipsAuthorCredits = styled.Text`
  color: #B7FFE9;
`
export const SeeMoreWrapper = styled.View`
  justify-content: center;
  align-items: center;
  flex: 0.3;
  padding: 10px;
`
export const SeeMoreButton = styled.TouchableOpacity`
  background-color: white;
  border-radius: 10px;
  width: 100px;
  height: 40px;
  justify-content: center;
  align-items: center
`
export const SeeMoreButtonText = styled.Text`
  color: #00587A;
  font-weight: bold;
`
