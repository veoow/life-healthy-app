import React from 'react'
import styled from 'styled-components/native'

import HeaderToolbar from './components/header-toolbar'
import Alarm from './components/alarm'
import Tips from './components/tips'
import BottomMenu from './components/bottom-menu'

function HomeView (props) {
  return (
    <HomeWrapper>
      <HeaderToolbar userInfo={props.user} />
      <ContentWrapper>
        <SectionTitle>
          Próximo Registro
        </SectionTitle>
        <Alarm />
        <SectionTitle>
          Dicas do Saudi!
        </SectionTitle>
        <Tips
          selectedTip={props.tips[(Math.random() * 4).toFixed()]}
        />
        <BottomMenu navigation={props.navigation} />
      </ContentWrapper>
    </HomeWrapper>
  )
}

export default HomeView

export const HomeWrapper = styled.View`
  width: 100%;
  height: 100%;
`
export const ContentWrapper = styled.View`
  width: 100%;
  height: 100%;
  padding-horizontal: 15;
  padding-top: 50;
  alignItems: center;
  flex: 1;
`
export const SectionTitle = styled.Text`
  font-size: 14;
  color: #00587A;
  text-align: left;
  width: 100%;
`
