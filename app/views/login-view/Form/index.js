import React, { Fragment, useContext } from 'react'

import TextInput from '../../../components/text-field'
import FormContext from '../../../context/form-context'
import { Text } from 'react-native'

function LoginForm () {
  const { values, errors, setFieldValue } = useContext(FormContext)

  return (
    <>
      <TextInput
        outlined='#69e6bf'
        rounded='10px'
        handleChange={text => setFieldValue('username', text)}
        value={values.username}
        placeholder='username...'
      />
      {
        errors.username &&
          <Text style={{ fontSize: 12, color: 'red', alignSelf: 'center' }}>
            {errors.username}
          </Text>
      }

      <TextInput
        outlined='#69e6bf'
        rounded='10px'
        handleChange={text => setFieldValue('password', text)}
        value={values.password}
        placeholder='password...'
        secureTextEntry
      />
      {
        errors.password &&
          <Text style={{ fontSize: 12, color: 'red', alignSelf: 'center' }}>
            {errors.password}
          </Text>
      }
    </>
  )
}

export default LoginForm
