import React, { useContext } from 'react'
import styled from 'styled-components/native'
import FormContext from '../../context/form-context'

import { Text, TouchableOpacity } from 'react-native'
import Button from '../../components/button'
import LoginForm from './Form'
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHeartbeat, faExclamationCircle } from '@fortawesome/free-solid-svg-icons'

function LoginView (props) {
  const { handleSubmit, isValid } = useContext(FormContext)
  return (
    <Wrapper>
      <HeaderWrapper>
        <TouchableOpacity>
          <FontAwesomeIcon icon={faExclamationCircle} size={32} color='red' />
        </TouchableOpacity>
      </HeaderWrapper>
      <BodyWrapper>
        <LogoWrapper>
          <FontAwesomeIcon icon={faHeartbeat} size={128} color='#69e6bf' />
          <TextTitle>Armor Healthy</TextTitle>
        </LogoWrapper>
        <TextInputWrapper>
          <LoginForm {...props} />
        </TextInputWrapper>
        <ButtonWrapper>
          <Button
            text='ENTRAR'
            backgroundColor='#69e6bf'
            color='#FFFFFF'
            rounded='5px'
            onPress={() => handleSubmit()}
            disabled={!isValid}
          />
        </ButtonWrapper>
      </BodyWrapper>
      <FooterTextWrapper>
        <Text>Don't have an account?</Text>
      </FooterTextWrapper>
    </Wrapper>
  )
}

export default LoginView

export const Wrapper = styled.View`
  background-color: white;
  height: 100%;
`
export const HeaderWrapper = styled.View`
  padding: 25px;
  justify-content: flex-start;
  align-items: flex-end;
`
export const BodyWrapper = styled.View`
  flex: 4;
  width: 100%;
  justify-content: center;
  align-items: center;
`
export const LogoWrapper = styled.View`
  padding: 30px;
  align-items: center;
`
export const TextTitle = styled.Text`
  font-size: 32;
  color: #69e6bf;
`
export const TextInputWrapper = styled.View`
  width: 90%;
`
export const ButtonWrapper = styled.View`
  width: 90%;
  padding: 10px;
`
export const FooterTextWrapper = styled.TouchableOpacity`
  justify-content: flex-end;
  padding: 10px;
  align-items: center;
  flex: 1;
`
