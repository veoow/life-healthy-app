import React from 'react'
import styled from 'styled-components'

import { View, Text } from 'react-native'

import HeaderToolbar from '../home-view/components/header-toolbar'
import TextInput from '../../components/text-field'
import FeelingsInput from '../../components/how-you-feel-input'
import CheckBoxInput from '../../components/checkbox-input'

import { faDumbbell } from '@fortawesome/free-solid-svg-icons'

function NewRecordView (props) {
  return (
    <Wrapper>
      <HeaderToolbar emergencyCall={props.emergencyCall} userInfo={props.user} />
      <PageTitleWrapper>
        <PageTitleText>Salvar registro</PageTitleText>
      </PageTitleWrapper>
      <ContentWrapper>
        <View>
          <TextInput
            outlined='#69e6bf'
            rounded='10px'
            handleChange={props.onChangeField('bloodSugar')}
            value={props.state.bloodSugar}
            placeholder='nível de açúcar no sangue...'
            keyboardType='number-pad'
          />
        </View>

        <FeelingsInput
          value={props.state.felling}
          handleChange={props.onChangeField('felling')}
        />

        <CheckBoxInput
          rounded={10}
          outlined
          text='você praticou algum exercício hoje?'
          icon={faDumbbell}
          iconColor='#35C196'
          value={props.state.workout}
          onChange={value => props.onChangeField('workout', value)}
        />

        <CheckBoxInput
          value={props.state.eatSomething}
          rounded={10}
          outlined
          text='comeu algo antes de tomar sua medicação?'
          onChange={value => props.onChangeField('eatSomething', value)}
        />

        <SubmitButton onPress={() => props.handleSubmit()}>
          <SubmitButtonText>registrar</SubmitButtonText>
        </SubmitButton>
      </ContentWrapper>
    </Wrapper>
  )
}

export default NewRecordView

const Wrapper = styled.View`
  flex: 1;
  align-items: center;
`
const PageTitleWrapper = styled.View`
  flex: 0.1;
  justify-content: center;
  align-items: center;
`
const PageTitleText = styled.Text`
  color: #43D9AB;
  font-weight: bold;
  font-size: 18px;
`
const ContentWrapper = styled.View`
  flex: 1;
  height: 100%;
  width: 100%;
  padding-vertical: 20;
  padding-horizontal: 25;
`
const SelectWrapper = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  border-color: #43D9AB;
  border-radius: 10px;
  border-width: 1px;
  margin-vertical: 10;
`
const SubmitButton = styled.TouchableOpacity`
  background-color: #43D9AB;
  border-radius: 10;
  width: 100%;
  height: 50px;
  justify-content: center;
  align-items: center;
  margin-vertical: 25;
`

const SubmitButtonText = styled.Text`
  color: white;
  font-size: 14px;
  font-weight: bold;
`
